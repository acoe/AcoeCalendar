package com.acoe.util;

import java.util.HashMap;
import java.util.Map;

public class Festival {
    private Map<String, String> gregorianFestival;
	private Map<String, String> chineseFestival;
	public Map<String, String> getGregorianFestival() {
		return gregorianFestival;
	}
	public void setGregorianFestival(Map<String, String> gregorianFestival) {
		this.gregorianFestival = gregorianFestival;
	}
	public Map<String, String> getChineseFestival() {
		return chineseFestival;
	}
	public void setChineseFestival(Map<String, String> chineseFestival) {
		this.chineseFestival = chineseFestival;
	}
	
	private static Festival festival = null;
	public static Festival getInstance() {
		if (festival == null) {
			festival = new Festival();
		}
		return festival;
	}
	public Festival() {
		gregorianFestival = new HashMap<String, String>();
		chineseFestival = new HashMap<String, String>();
		gregorianFestival.put("0101", "元旦");
		gregorianFestival.put("0214", "情人节");
		gregorianFestival.put("0308", "妇女节");
		gregorianFestival.put("0312", "植树节");
		gregorianFestival.put("0401", "愚人节");
		gregorianFestival.put("0405", "清明节");
		gregorianFestival.put("0501", "劳动节");
		gregorianFestival.put("05secondSunday", "母亲节"); // 每年5月的第二个星期日
		gregorianFestival.put("0512", "护士节");
		gregorianFestival.put("0601", "儿童节");
		gregorianFestival.put("06thirdSunday", "父亲节"); // 每年6月的第三个星期日
		gregorianFestival.put("0701", "建党节");
		gregorianFestival.put("0801", "建军节");
		gregorianFestival.put("0910", "教师节");
		gregorianFestival.put("1001", "国庆节");
		gregorianFestival.put("1012", "感恩节");
		gregorianFestival.put("1224", "平安夜");
		gregorianFestival.put("1225", "圣诞节");
		chineseFestival.put("正月初一", "春节");
		chineseFestival.put("正月十五", "元宵节");
		chineseFestival.put("五月初五", "端午节");
		chineseFestival.put("七月初七", "七夕节");
		chineseFestival.put("七月十五", "中元节");
		chineseFestival.put("八月十五", "中秋节");
		chineseFestival.put("九月初九", "重阳节");
		chineseFestival.put("十月初一", "寒衣节");
		chineseFestival.put("十月十五", "下元节");
		chineseFestival.put("腊月初八", "腊八节");
		chineseFestival.put("腊月廿三", "祭灶节");
		chineseFestival.put("腊月廿九或三十", "除夕"); // 农历年的最后一天
	}
}
