package com.acoe.view;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

public class Surface {
    private String tag = "Surface";
	public float density;
	public int width;
	public int height;
	public float weekHeight;
	public float dayWidth;
	public float dayHeight;
	public float borderWidth;
	public int bgColor = Color.parseColor("#FFFFFF");
	public int textColor = Color.BLACK;
	public int weekTitleBgColor = Color.parseColor("#E3D39C");
	public int borderColor = Color.parseColor("#CCCCCC");
	public int weekendColor = Color.parseColor("#008000");
	public int dayDownColor = Color.parseColor("#CCFFFF");
	public int daySelectedColor = Color.parseColor("#80CCFF");
	public int greyDateColor = Color.parseColor("#808080"); // 非当前月的工作日日期颜色
	public int greenDateColor = Color.parseColor("#008000"); // 当前月周末日期颜色
	public int greenLessDateColor = Color.parseColor("#80FF80"); // 非当前月的月末日期颜色
	public int redDateColor = Color.parseColor("#FF0000"); // 当前月节日日期颜色
	public int redLessDateColor = Color.parseColor("#FF8080"); // 非当前月节日日期颜色
	public int todayMarkColor = Color.parseColor("#00FF00"); // 今天日期的标记颜色
	public Paint borderPaint;
	public Paint weekTitleBgPaint;
	public Paint weekPaint;
	public Paint dayPaint;
	public Paint festivalPaint;
	public Paint dayBgPaint;
	public Paint todayMarkPaint;
	public Path borderPath;
	public String[] weekTexts = { "星期日", "星期一", "星期二", "星期三",
			"星期四", "星期五", "星期六"};
	
	public void init() {
		float temp =  height / 13;
		weekHeight = (float) ((temp + temp * 0.3f) * 0.7);
		dayHeight = (height - weekHeight) / 6f;
		dayWidth = width / 7;
		borderPaint = new Paint();
		borderPaint.setColor(borderColor);
		borderPaint.setStyle(Paint.Style.STROKE);
		borderWidth = (float) (0.7 * density);
		borderWidth = borderWidth < 1 ? 1 : borderWidth;
		borderPaint.setStrokeWidth(borderWidth);
		weekTitleBgPaint = new Paint();
		weekTitleBgPaint.setColor(weekTitleBgColor);
		weekTitleBgPaint.setStyle(Paint.Style.FILL);
		weekPaint = new Paint();
		weekPaint.setColor(textColor);
		weekPaint.setAntiAlias(true);
		float weekSize = weekHeight * 0.6f;
		weekPaint.setTextSize(weekSize);
		dayPaint = new Paint();
		dayPaint.setColor(textColor);
		dayPaint.setAntiAlias(true);
		float dayTextSize = dayHeight * 0.5f / 2;
		dayPaint.setTextSize(dayTextSize);
		festivalPaint = new Paint();
		festivalPaint.setColor(Color.RED);
		festivalPaint.setAntiAlias(true);
		festivalPaint.setTextSize(dayTextSize * 0.8f); // 节日字体大小和日期天相同
		dayBgPaint = new Paint();
		dayBgPaint.setAntiAlias(true);
		dayBgPaint.setStyle(Paint.Style.FILL);
		dayBgPaint.setColor(daySelectedColor);
		todayMarkPaint = new Paint();
		todayMarkPaint.setAntiAlias(true);
		todayMarkPaint.setStyle(Paint.Style.FILL);
		todayMarkPaint.setColor(todayMarkColor);
		borderPath = new Path();
		borderPath.rLineTo(width, 0);
		borderPath.moveTo(0, weekHeight);
		borderPath.rLineTo(width, 0);
		for (int i = 0; i < 6; i++) {
			borderPath.moveTo(0, weekHeight + dayHeight * i);
			borderPath.rLineTo(width, 0);
			borderPath.moveTo(dayWidth * i, 0);
			borderPath.rLineTo(0, height);
		}
		borderPath.moveTo(dayWidth * 6, 0);
		borderPath.lineTo(dayWidth * 6, height);
	}
}
