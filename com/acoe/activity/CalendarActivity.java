package com.acoe.activity;




import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.acoe.view.AcoeCalendarView;

public class CalendarActivity extends Activity {
    private String tag = "CalendarActivity";
	private int calendarMode = 0; // 日历查看模式，0：阳历；1：阴历；2：阴历阳历都显示
	private Button todayButton;
	private ImageButton lastMonthButton;
	private ImageButton nextMonthButton;
	private AcoeCalendarView acoeCalendarView;
	private TextView detailDateText;
	private TextView curYearText;
	private TextView curMonthText;
	private OnClickListener ocl;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		initView();
		initData();
		setListener();
	}
	
	private void initView() {
		todayButton = (Button) findViewById(R.id.todayButton);
		detailDateText = (TextView) findViewById(R.id.detailDateText);
		lastMonthButton = (ImageButton) findViewById(R.id.lastMonthButton);
		nextMonthButton = (ImageButton) findViewById(R.id.nextMonthButton);
		acoeCalendarView = (AcoeCalendarView) findViewById(R.id.calendarView);
		curYearText = (TextView) findViewById(R.id.yearText);
		curMonthText = (TextView) findViewById(R.id.monthText);
	}
	
	private void initData() {
		detailDateText.setText(acoeCalendarView.getCurDate());
		curYearText.setText(acoeCalendarView.getCurYear());
		curMonthText.setText(acoeCalendarView.getCurMonth());
	}
	
	private void setListener() {
		ocl = new OnClickListener() {

			@Override
			public void onClick(View view) {
				switch (view.getId()) {
				case R.id.lastMonthButton:
					acoeCalendarView.clickLastMonth();
					initData();
					break;
				case R.id.nextMonthButton:
					acoeCalendarView.clickNextMonth();
					initData();
					break;
				case R.id.todayButton:
					acoeCalendarView.clickToday();
					initData();
					break;
				case R.id.calendarView:
					initData();
					break;
				}
			}
			
		};
		todayButton.setOnClickListener(ocl);
		lastMonthButton.setOnClickListener(ocl);
		nextMonthButton.setOnClickListener(ocl);
        acoeCalendarView.setOnClickListener(ocl);
	}
}